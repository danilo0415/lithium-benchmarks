id = eu.woju.android.packages.hud
title = HUD
description = HUDHeads-up display for your car
apk_url = https://f-droid.org/repo/eu.woju.android.packages.hud_10.apk
source_url = https://f-droid.org/repo/eu.woju.android.packages.hud_10_src.tar.gz
