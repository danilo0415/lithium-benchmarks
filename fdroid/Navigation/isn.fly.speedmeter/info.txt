id = isn.fly.speedmeter
title = SpeedMeter
description = SpeedMeterLightweight speed meter
apk_url = https://f-droid.org/repo/isn.fly.speedmeter_9.apk
source_url = https://f-droid.org/repo/isn.fly.speedmeter_9_src.tar.gz
