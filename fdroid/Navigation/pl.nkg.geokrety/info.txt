id = pl.nkg.geokrety
title = GeoKrety Logger
description = GeoKrety LoggerGeocaching client
apk_url = https://f-droid.org/repo/pl.nkg.geokrety_29.apk
source_url = https://f-droid.org/repo/pl.nkg.geokrety_29_src.tar.gz
