id = de.onyxbits.geobookmark
title = Geo Bookmark
description = Geo BookmarkSave timestamped geo-bookmarks
apk_url = https://f-droid.org/repo/de.onyxbits.geobookmark_2.apk
source_url = https://f-droid.org/repo/de.onyxbits.geobookmark_2_src.tar.gz
