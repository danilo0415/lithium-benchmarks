id = org.gfd.gsmlocation
title = GSMLocationBackend
description = GSMLocationBackendOpenCellID location provider for UnifiedNlp
apk_url = https://f-droid.org/repo/org.gfd.gsmlocation_13.apk
source_url = https://f-droid.org/repo/org.gfd.gsmlocation_13_src.tar.gz
