id = org.penghuang.tools.rotationlock
title = Rotation Lock
description = Rotation LockStop screen auto-rotation
apk_url = https://f-droid.org/repo/org.penghuang.tools.rotationlock_5.apk
source_url = https://f-droid.org/repo/org.penghuang.tools.rotationlock_5_src.tar.gz
