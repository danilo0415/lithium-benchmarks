id = org.fedorahosted.freeotp
title = FreeOTP
description = FreeOTPTwo-factor authentication
apk_url = https://f-droid.org/repo/org.fedorahosted.freeotp_11.apk
source_url = https://f-droid.org/repo/org.fedorahosted.freeotp_11_src.tar.gz
