id = pl.narfsoftware.thermometer
title = Thermometer Extended
description = Thermometer ExtendedAmbient conditions with charts
apk_url = https://f-droid.org/repo/pl.narfsoftware.thermometer_29.apk
source_url = https://f-droid.org/repo/pl.narfsoftware.thermometer_29_src.tar.gz
