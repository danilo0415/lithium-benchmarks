id = org.xapek.andiodine
title = AndIodine
description = AndIodineData connection through DNS tunneling
apk_url = https://f-droid.org/repo/org.xapek.andiodine_1.apk
source_url = https://f-droid.org/repo/org.xapek.andiodine_1_src.tar.gz
