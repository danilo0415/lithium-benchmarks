id = com.willhauck.linconnectclient
title = LinConnect
description = LinConnectMirror notifications to desktop
apk_url = https://f-droid.org/repo/com.willhauck.linconnectclient_7.apk
source_url = https://f-droid.org/repo/com.willhauck.linconnectclient_7_src.tar.gz
