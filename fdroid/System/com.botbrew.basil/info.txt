id = com.botbrew.basil
title = BotBrew Basil
description = BotBrew BasilDebian package management
apk_url = https://f-droid.org/repo/com.botbrew.basil_24.apk
source_url = https://f-droid.org/repo/com.botbrew.basil_24_src.tar.gz
