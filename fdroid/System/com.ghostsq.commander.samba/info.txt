id = com.ghostsq.commander.samba
title = Ghost Commander - Samba plugin
description = Ghost Commander - Samba pluginAccess files on the network
apk_url = https://f-droid.org/repo/com.ghostsq.commander.samba_51.apk
source_url = https://f-droid.org/repo/com.ghostsq.commander.samba_51_src.tar.gz
