id = org.nick.cryptfs.passwdmanager
title = Cryptfs Password
description = Cryptfs PasswordDisk encryption password changer
apk_url = https://f-droid.org/repo/org.nick.cryptfs.passwdmanager_1210.apk
source_url = https://f-droid.org/repo/org.nick.cryptfs.passwdmanager_1210_src.tar.gz
