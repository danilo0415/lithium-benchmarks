id = net.iowaline.dotdash
title = DotDash Keyboard
description = DotDash KeyboardMorse code keyboard
apk_url = https://f-droid.org/repo/net.iowaline.dotdash_10.apk
source_url = https://f-droid.org/repo/net.iowaline.dotdash_10_src.tar.gz
