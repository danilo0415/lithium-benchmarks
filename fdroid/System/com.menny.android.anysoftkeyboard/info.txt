id = com.menny.android.anysoftkeyboard
title = AnySoftKeyboard
description = AnySoftKeyboardAlternative keyboard
apk_url = https://f-droid.org/repo/com.menny.android.anysoftkeyboard_102.apk
source_url = https://f-droid.org/repo/com.menny.android.anysoftkeyboard_102_src.tar.gz
