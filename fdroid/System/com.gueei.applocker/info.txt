id = com.gueei.applocker
title = AppLocker
description = AppLockerLockdown your apps
apk_url = https://f-droid.org/repo/com.gueei.applocker_3.apk
source_url = https://f-droid.org/repo/com.gueei.applocker_3_src.tar.gz
