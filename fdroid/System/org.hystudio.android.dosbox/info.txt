id = org.hystudio.android.dosbox
title = aDosBox
description = aDosBoxDOSBox x86 emulator port
apk_url = https://f-droid.org/repo/org.hystudio.android.dosbox_20500.apk
source_url = https://f-droid.org/repo/org.hystudio.android.dosbox_20500_src.tar.gz
