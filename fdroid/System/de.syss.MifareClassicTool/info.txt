id = de.syss.MifareClassicTool
title = Mifare Classic Tool
description = Mifare Classic ToolAnalyse a type of NFC tag
apk_url = https://f-droid.org/repo/de.syss.MifareClassicTool_24.apk
source_url = https://f-droid.org/repo/de.syss.MifareClassicTool_24_src.tar.gz
