id = org.fdroid.fdroid
title = F-Droid
description = F-DroidApplication manager
apk_url = https://f-droid.org/repo/org.fdroid.fdroid_660.apk
source_url = https://f-droid.org/repo/org.fdroid.fdroid_660_src.tar.gz
