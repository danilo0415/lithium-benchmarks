id = org.nathan.jf.build.prop.editor
title = build.prop Editor
description = build.prop EditorTweak system properties
apk_url = https://f-droid.org/repo/org.nathan.jf.build.prop.editor_3.apk
source_url = https://f-droid.org/repo/org.nathan.jf.build.prop.editor_3_src.tar.gz
