id = org.androidsoft.app.permission
title = Permission Friendly Apps
description = Permission Friendly AppsRank apps by permissions
apk_url = https://f-droid.org/repo/org.androidsoft.app.permission_12.apk
source_url = https://f-droid.org/repo/org.androidsoft.app.permission_12_src.tar.gz
