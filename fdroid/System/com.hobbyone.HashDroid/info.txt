id = com.hobbyone.HashDroid
title = Hash Droid
description = Hash DroidVerify file integrity
apk_url = https://f-droid.org/repo/com.hobbyone.HashDroid_18.apk
source_url = https://f-droid.org/repo/com.hobbyone.HashDroid_18_src.tar.gz
