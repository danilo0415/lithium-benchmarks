id = com.color.colornamer
title = Color Namer
description = Color NamerNames for colours
apk_url = https://f-droid.org/repo/com.color.colornamer_8.apk
source_url = https://f-droid.org/repo/com.color.colornamer_8_src.tar.gz
