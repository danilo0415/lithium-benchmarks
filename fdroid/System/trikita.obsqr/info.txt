id = trikita.obsqr
title = Obsqr
description = ObsqrQR code scanner
apk_url = https://f-droid.org/repo/trikita.obsqr_9.apk
source_url = https://f-droid.org/repo/trikita.obsqr_9_src.tar.gz
