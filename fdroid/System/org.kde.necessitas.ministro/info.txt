id = org.kde.necessitas.ministro
title = Ministro
description = MinistroNeeded by apps written in Qt
apk_url = https://f-droid.org/repo/org.kde.necessitas.ministro_6.apk
source_url = https://f-droid.org/repo/org.kde.necessitas.ministro_6_src.tar.gz
