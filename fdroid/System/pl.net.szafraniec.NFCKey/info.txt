id = pl.net.szafraniec.NFCKey
title = NFC Key
description = NFC KeyUnlock KeePass database with NFC
apk_url = https://f-droid.org/repo/pl.net.szafraniec.NFCKey_16.apk
source_url = https://f-droid.org/repo/pl.net.szafraniec.NFCKey_16_src.tar.gz
