id = com.nolanlawson.apptracker
title = App Tracker
description = App TrackerTrack your app usage
apk_url = https://f-droid.org/repo/com.nolanlawson.apptracker_10.apk
source_url = https://f-droid.org/repo/com.nolanlawson.apptracker_10_src.tar.gz
