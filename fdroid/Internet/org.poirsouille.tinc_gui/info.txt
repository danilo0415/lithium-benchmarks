id = org.poirsouille.tinc_gui
title = Tinc
description = TincPort of Tinc VPN
apk_url = https://f-droid.org/repo/org.poirsouille.tinc_gui_8.apk
source_url = https://f-droid.org/repo/org.poirsouille.tinc_gui_8_src.tar.gz
