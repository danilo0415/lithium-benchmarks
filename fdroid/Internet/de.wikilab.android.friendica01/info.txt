id = de.wikilab.android.friendica01
title = Friendica
description = FriendicaSocial networking
apk_url = https://f-droid.org/repo/de.wikilab.android.friendica01_8.apk
source_url = https://f-droid.org/repo/de.wikilab.android.friendica01_8_src.tar.gz
