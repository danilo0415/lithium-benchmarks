id = org.quantumbadger.redreader
title = RedReader Beta
description = RedReader BetaClient for reddit.com
apk_url = https://f-droid.org/repo/org.quantumbadger.redreader_48.apk
source_url = https://f-droid.org/repo/org.quantumbadger.redreader_48_src.tar.gz
