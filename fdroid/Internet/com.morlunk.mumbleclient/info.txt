id = com.morlunk.mumbleclient
title = Plumble
description = PlumbleVoice chat for Mumble servers
apk_url = https://f-droid.org/repo/com.morlunk.mumbleclient_64.apk
source_url = https://f-droid.org/repo/com.morlunk.mumbleclient_64_src.tar.gz
