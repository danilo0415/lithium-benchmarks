id = com.xabber.androiddev
title = Xabber
description = XabberInstant messaging client
apk_url = https://f-droid.org/repo/com.xabber.androiddev_78.apk
source_url = https://f-droid.org/repo/com.xabber.androiddev_78_src.tar.gz
