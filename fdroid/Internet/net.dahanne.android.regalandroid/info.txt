id = net.dahanne.android.regalandroid
title = ReGalAndroid
description = ReGalAndroidViewer for remote image galleries
apk_url = https://f-droid.org/repo/net.dahanne.android.regalandroid_5.apk
source_url = https://f-droid.org/repo/net.dahanne.android.regalandroid_5_src.tar.gz
