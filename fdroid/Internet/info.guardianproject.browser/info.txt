id = info.guardianproject.browser
title = Orweb
description = OrwebPrivacy-enhanced browser
apk_url = https://f-droid.org/repo/info.guardianproject.browser_24.apk
source_url = https://f-droid.org/repo/info.guardianproject.browser_24_src.tar.gz
