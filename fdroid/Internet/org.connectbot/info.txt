id = org.connectbot
title = ConnectBot
description = ConnectBotSSH and local shell client
apk_url = https://f-droid.org/repo/org.connectbot_365.apk
source_url = https://f-droid.org/repo/org.connectbot_365_src.tar.gz
