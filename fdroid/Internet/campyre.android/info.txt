id = campyre.android
title = Campyre
description = CampyreClient for Campfire
apk_url = https://f-droid.org/repo/campyre.android_7.apk
source_url = https://f-droid.org/repo/campyre.android_7_src.tar.gz
