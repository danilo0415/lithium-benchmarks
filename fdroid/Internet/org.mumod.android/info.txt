id = org.mumod.android
title = Mustard {MOD}
description = Mustard {MOD}Microblogging client
apk_url = https://f-droid.org/repo/org.mumod.android_168.apk
source_url = https://f-droid.org/repo/org.mumod.android_168_src.tar.gz
