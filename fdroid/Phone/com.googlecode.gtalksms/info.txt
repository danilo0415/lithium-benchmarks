id = com.googlecode.gtalksms
title = GTalkSMS
description = GTalkSMSControl your phone via XMPP
apk_url = https://f-droid.org/repo/com.googlecode.gtalksms_64.apk
source_url = https://f-droid.org/repo/com.googlecode.gtalksms_64_src.tar.gz
