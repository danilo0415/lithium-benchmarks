id = it.iiizio.epubator
title = ePUBator
description = ePUBatorOffline PDF to ePUB convertor
apk_url = https://f-droid.org/repo/it.iiizio.epubator_10.apk
source_url = https://f-droid.org/repo/it.iiizio.epubator_10_src.tar.gz
