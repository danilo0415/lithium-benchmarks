id = org.sufficientlysecure.viewer
title = Document Viewer
description = Document ViewerViewer for many document formats
apk_url = https://f-droid.org/repo/org.sufficientlysecure.viewer_2200.apk
source_url = https://f-droid.org/repo/org.sufficientlysecure.viewer_2200_src.tar.gz
