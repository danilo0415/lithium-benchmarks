id = org.torproject.android
title = Orbot
description = OrbotTor (anonymity) client
apk_url = https://f-droid.org/repo/org.torproject.android_105.apk
source_url = https://f-droid.org/repo/org.torproject.android_105_src.tar.gz
