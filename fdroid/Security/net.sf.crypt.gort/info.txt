id = net.sf.crypt.gort
title = Gort
description = GortTwo factor auth client for Barada
apk_url = https://f-droid.org/repo/net.sf.crypt.gort_8.apk
source_url = https://f-droid.org/repo/net.sf.crypt.gort_8_src.tar.gz
