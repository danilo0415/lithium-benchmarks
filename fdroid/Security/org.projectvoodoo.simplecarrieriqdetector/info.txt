id = org.projectvoodoo.simplecarrieriqdetector
title = Voodoo CarrierIQ Detector
description = Voodoo CarrierIQ DetectorCheck for spyware on the device
apk_url = https://f-droid.org/repo/org.projectvoodoo.simplecarrieriqdetector_16.apk
source_url = https://f-droid.org/repo/org.projectvoodoo.simplecarrieriqdetector_16_src.tar.gz
