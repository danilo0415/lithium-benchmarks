id = de.jurihock.voicesmith
title = Voicesmith
description = VoicesmithReal-time voice changer
apk_url = https://f-droid.org/repo/de.jurihock.voicesmith_10.apk
source_url = https://f-droid.org/repo/de.jurihock.voicesmith_10_src.tar.gz
