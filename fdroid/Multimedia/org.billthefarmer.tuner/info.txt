id = org.billthefarmer.tuner
title = Tuner
description = TunerMusical instrument pitch tuning
apk_url = https://f-droid.org/repo/org.billthefarmer.tuner_102.apk
source_url = https://f-droid.org/repo/org.billthefarmer.tuner_102_src.tar.gz
