id = jpf.android.magiadni
title = MagiaDNI
description = MagiaDNIOCR reader for Spanish id cards
apk_url = https://f-droid.org/repo/jpf.android.magiadni_7.apk
source_url = https://f-droid.org/repo/jpf.android.magiadni_7_src.tar.gz
