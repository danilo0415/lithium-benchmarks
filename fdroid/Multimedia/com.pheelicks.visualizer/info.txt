id = com.pheelicks.visualizer
title = Visualizer
description = VisualizerDisplay multimedia visualisations
apk_url = https://f-droid.org/repo/com.pheelicks.visualizer_1.apk
source_url = https://f-droid.org/repo/com.pheelicks.visualizer_1_src.tar.gz
