id = net.chilon.matt.teacup
title = TeaCup
description = TeaCupConfigurable music widget
apk_url = https://f-droid.org/repo/net.chilon.matt.teacup_3.apk
source_url = https://f-droid.org/repo/net.chilon.matt.teacup_3_src.tar.gz
