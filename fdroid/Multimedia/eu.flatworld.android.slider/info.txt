id = eu.flatworld.android.slider
title = Slider
description = SliderBasic musical instrument
apk_url = https://f-droid.org/repo/eu.flatworld.android.slider_3.apk
source_url = https://f-droid.org/repo/eu.flatworld.android.slider_3_src.tar.gz
