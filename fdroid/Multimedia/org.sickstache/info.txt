id = org.sickstache
title = SickStache
description = SickStacheSickbeard client
apk_url = https://f-droid.org/repo/org.sickstache_42.apk
source_url = https://f-droid.org/repo/org.sickstache_42_src.tar.gz
