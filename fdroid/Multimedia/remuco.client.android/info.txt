id = remuco.client.android
title = Remuco
description = RemucoRemote control for media players
apk_url = https://f-droid.org/repo/remuco.client.android_1.apk
source_url = https://f-droid.org/repo/remuco.client.android_1_src.tar.gz
