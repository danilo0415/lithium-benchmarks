id = org.ncrmnt.nettts
title = NetTTS
description = NetTTSText-to-Speech over network
apk_url = https://f-droid.org/repo/org.ncrmnt.nettts_3.apk
source_url = https://f-droid.org/repo/org.ncrmnt.nettts_3_src.tar.gz
