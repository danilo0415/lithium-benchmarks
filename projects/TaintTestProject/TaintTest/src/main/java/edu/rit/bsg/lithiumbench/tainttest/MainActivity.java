package edu.rit.bsg.lithiumbench.tainttest;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MainActivity extends ActionBarActivity {
    String loc;
    String mOtherField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            this.loc = location.toString();
        } else {
            this.loc = "not tainted";
        }
        this.mOtherField = "not tainted";
    }

    @Override
    protected void onDestroy() {
        try {
            StringBuilder str = new StringBuilder();
            str.append(URLEncoder.encode(mOtherField));
            str.append(URLEncoder.encode(loc));
            Log.i("Ringtone Maker", str.toString());

            byte[] bytes = str.toString().getBytes("UTF8");
            Log.i("Ringtone Maker 2", bytes.toString());
            BasicHttpParams localBasicHttpParams = new BasicHttpParams();
            DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient(localBasicHttpParams);
            HttpPost localHttpPost = new HttpPost("http://google.com");
            localHttpPost.setEntity(new ByteArrayEntity(str.toString().getBytes("UTF8")));

            Log.i("Ringtone Maker", "Executing request");
            HttpResponse localHttpResponse = localDefaultHttpClient.execute(localHttpPost);
            Log.i("Ringtone Maker", "Response: " + localHttpResponse.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

}
