package edu.rit.bsg.lithiumbench.systemcalls;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service implements LocationListener {
    private static final String TAG = "MyService";
    private LocationManager locationManager;
    private boolean started = false;

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "onLocationChanged called with started = " + started);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        started = true;
        Log.i(TAG, "onCreate called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        locationManager.removeUpdates(this);
        started = false;
        Log.i(TAG, "onDestroy Service called");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
