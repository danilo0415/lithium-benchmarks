package edu.rit.bsg.lithiumbench.systemcalls;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/**
 * Created by danilo04 on 8/6/14.
 */
public class Monitor implements LocationListener {

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
