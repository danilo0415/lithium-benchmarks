package edu.rit.bsg.lithium.benignapplication1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BenignReceiver extends BroadcastReceiver {
    @Override
    public void onReceive (Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, BenignService.class);
        serviceIntent.putExtra("data", intent.getStringExtra("data"));
        serviceIntent.putExtra("url", intent.getStringExtra("url"));
        Log.i("BenignReceiver", "Data: " + intent.getStringExtra("data"));
        context.startService(serviceIntent);
    }
}

