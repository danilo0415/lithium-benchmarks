package edu.rit.bsg.lithiumbench.rotationbug.app;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by danilo04 on 7/23/14.
 */
public class Data implements Parcelable {
    String text;
    String text2;

    public Data(String text, String text2) {
        this.text = text;
        this.text2 = text2;
    }

    public Data(Parcel in) {
        String[] data = new String[2];

        in.readStringArray(data);
        this.text = data[0];
        this.text2 = data[1];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getText() {
        return text;
    }

    public String getText2() {
        return text2;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[] {
                text, text2
        });
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
