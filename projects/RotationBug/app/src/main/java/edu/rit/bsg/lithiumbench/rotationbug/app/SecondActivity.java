package edu.rit.bsg.lithiumbench.rotationbug.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


public class SecondActivity extends ActionBarActivity {
    String text;
    Data data;
    static final String TAG = "SecondActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Bundle extras = getIntent().getExtras();
        Log.i(TAG, extras.getString("text"));
        text = extras.getString("text");
        data = (Data) extras.getParcelable("data");

        Log.i(TAG, "SecondActivity.onCreate called");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i(TAG, "SecondActivity.onLowMemory called");
    }

    @Override
    public void onTrimMemory(int level) {
        Log.i(TAG, "SecondActivity.onTrimMemory called");
    }

    public void onToastClick(View button) {
        Toast.makeText(this, text.toUpperCase() + " text: " + data.getText(), Toast.LENGTH_LONG).show();
    }

    public void onClickOpen(View button) {
        startActivity(new Intent(this, ThirdActivity.class));
    }

    protected void onStop() {
        super.onStop();
        Log.i(TAG, "SecondActivity.onStop called");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "SecondActivity.onDestroy called");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
