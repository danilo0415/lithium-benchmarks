package edu.rit.bsg.lithiumbench.intentnull.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class ThirdActivity extends ActionBarActivity {

    private static final String TAG = "ThirdActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        Log.i(TAG, "ThirdActivity.onStart called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        Toast.makeText(this, name, Toast.LENGTH_LONG).show();
        Log.i(TAG, "ThirdActivity.onStart called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "ThirdActivity.onResume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "ThirdActivity.onPause called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "ThirdActivity.onStop called");
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        Log.i(TAG, "ThirdActivity.onSaveInstanceState called");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.third, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
