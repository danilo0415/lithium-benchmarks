package edu.rit.bsg.lithiumbench.bindservicegraph;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
    private static final String TAG = "Service";
    private String val;

    public class MyBinder extends Binder {
        MyService getService() {
            return MyService.this;
        }
    }

    MyBinder binder = new MyBinder();


    public MyService() {
    }

    public void check() {
        val = "some string";
    }

    public void clean() {
	val = null;
    }

    public String getVal() {
        return val;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind service called");

        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "onUnbind service called");
	Log.i(TAG, val.toLowerCase());
        return false;
    }
}
