package edu.rit.bsg.lithiumbench.bindservicegraph;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = "Activity";

    private MyService s = null;
    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder binder) {
            MyService.MyBinder b = (MyService.MyBinder) binder;
            s = b.getService();
            Log.i(TAG, "Service connected");
            Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT)
                    .show();
        }

        public void onServiceDisconnected(ComponentName className) {
            s = null;
            Log.i(TAG, "Service disconnected");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindService(new Intent(this, MyService.class), mConnection, Context.BIND_AUTO_CREATE);
        Log.i(TAG, "onCreate was called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart was called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume was called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        s.check();
        Toast.makeText(this, s.getVal().toLowerCase(), Toast.LENGTH_LONG).show();
        Log.i(TAG, "onPause was called");
    }

    @Override
    protected void onStop() {
        super.onStop();
	s.clean();
        unbindService(mConnection);
        Log.i(TAG, "onStop was called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy was called");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
