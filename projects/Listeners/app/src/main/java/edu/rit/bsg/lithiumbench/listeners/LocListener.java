package edu.rit.bsg.lithiumbench.listeners;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by danilo04 on 8/10/14.
 */
public class LocListener implements LocationListener {
    String b;
    String b2;
    String b3;
    static final String TAG = "LocListener";

    @Override
    public void onLocationChanged(Location location) {
        b2 = "Hello";
        Log.i(TAG, "B has: " + b.toLowerCase());
        Log.i(TAG, "B2 has: " + b2.toLowerCase());
        if (b3 != null) {
            Log.i(TAG, "B3 has: " + b3.toLowerCase());
        }

        Log.i(TAG, "Calling onLocationChanged");
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
