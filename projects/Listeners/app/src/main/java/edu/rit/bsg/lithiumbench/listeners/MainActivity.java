package edu.rit.bsg.lithiumbench.listeners;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = "Activity";
    private int a = 5;
    private String b;
    private String b2;
    private String b3;
    private ExecutorService s1;
    private ExecutorService s2;
    private TextView textView;
    private LocationListener locListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            b2 = "Hello";
            Log.i(TAG, "B has: " + b.toLowerCase());
            Log.i(TAG, "B2 has: " + b2.toLowerCase());
            if (b3 != null) {
                Log.i(TAG, "B3 has: " + b3.toLowerCase());
            }

            if (!s1.isShutdown()) {
                s1.submit(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }

            if (s2.isShutdown()) {
                Log.i(TAG, "Test");
            }

            s2.submit(new Runnable() {
                @Override
                public void run() {

                }
            });

            Log.i(TAG, "Calling onLocationChanged");
            textView.setText("This is set from location update");
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Log.i(TAG, "onCreate called");
        b = "Hello";
	    b2 = "Hello2";
        b3 = "Hello3";
        s1 = Executors.newFixedThreadPool(1);
        s2 = Executors.newFixedThreadPool(2);
    }

    @Override
    protected void onStart() {
        super.onStart();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
        Log.i(TAG, "onStart called");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void onStopListening(View button) {
        if (a > 5) {
            locationManager.removeUpdates(locListener);
        }
       // startActivity(new Intent(this, SecondActivity.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(locListener);
        b = null;
    	b2 = null;
        b3 = null;
        s1.shutdown();
        s2.shutdown();
        Log.i(TAG, "onPause called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop called");
    }

     @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy called");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
