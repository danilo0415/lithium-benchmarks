package edu.rit.bsg.lithiumbench.iccfgtest.app;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService2 extends Service {
    private static final String TAG = "MyService2";
    public MyService2() {
        Log.i(TAG, "MyService2.constructor called");
    }

    @Override
    public void onCreate () {
        super.onCreate();
        Log.i(TAG, "MyService2.onCreate called");
    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.i(TAG, "MyService2.onStartCommand called");

        return START_STICKY;
    }



    @Override
    public void onDestroy () {
        super.onDestroy();
        Log.i(TAG, "MyService2.onCreate called");
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "MyService2.onBind called");
        return null;
    }
}
