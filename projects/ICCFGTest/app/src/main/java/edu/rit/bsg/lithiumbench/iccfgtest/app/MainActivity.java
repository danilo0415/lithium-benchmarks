package edu.rit.bsg.lithiumbench.iccfgtest.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ActionBarActivity {
    public static class TestTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
           /* try {
                Thread.sleep(700);
            } catch (InterruptedException e) {
            }*/
            Log.i("TestTask", "doInBackground called");
            return "Test";
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i("TestTask", "onPostExecute called");
        }
    }

    private TestTask task;
    private static final String TAG = "MainActivity";
    private ServiceConnection mConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.i(TAG, "MainActivity.onServiceConnected called");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "MainActivity.onCreate called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent i = new Intent(this, MyService.class);
        startService(i);
        bindService(i, mConn, Context.BIND_AUTO_CREATE);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "MainActivity.runnable1 called");
            }
        });
        task = new TestTask();
        task.execute();
        Log.i(TAG, "MainActivity.onStart called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "MainActivity.onResume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "MainActivity.onPause called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "MainActivity.onStop called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "MainActivity.onDestroy called");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
