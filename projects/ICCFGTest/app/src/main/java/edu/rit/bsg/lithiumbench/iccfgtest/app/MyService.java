package edu.rit.bsg.lithiumbench.iccfgtest.app;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
    private static final String TAG = "MyService";
    public MyService() {
        Log.i(TAG, "MyService.constructor called");
    }

    @Override
    public void onCreate () {
        super.onCreate();
        Intent i = new Intent(this, MyService2.class);
        startService(i);
        Log.i(TAG, "MyService.onCreate called");
    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.i(TAG, "MyService.onStartCommand called");

        return START_STICKY;
    }



    @Override
    public void onDestroy () {
        super.onDestroy();
        Log.i(TAG, "MyService.onCreate called");
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "MyService.onBind called");
        return null;
    }
}
