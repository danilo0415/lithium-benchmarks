package edu.rit.bsg.lithiumbench.guionpause.app;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = "MainActivity";
    private String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i(TAG, "MainActivity.onCreate called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "MainActivity.onStart called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        s = "hello";
        Log.i(TAG, "MainActivity.onResume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        s = null;
        Log.i(TAG, "MainActivity.onPause called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "MainActivity.onStop called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "MainActivity.onDestroy called");
    }

    public void onCheckClick(View button) {
        Log.i(TAG, "MainActivity.onCheckClick called");
        Log.i(TAG, "Content of s: " + s.substring(1));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        Log.i(TAG, "MainActivity.onCreateOptionsMenu called");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        Log.i(TAG, "MainActivity.onOptionsItemSelected called");
        return super.onOptionsItemSelected(item);
    }

}
