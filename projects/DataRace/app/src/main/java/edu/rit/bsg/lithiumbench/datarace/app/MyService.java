package edu.rit.bsg.lithiumbench.datarace.app;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
    public class MyBinder extends Binder {
        MyService getService() {
            return MyService.this;
        }
    }
    private MyBinder binder = new MyBinder();

    public MyService() {
    }

    @Override
    public void onCreate() {
        Log.i("Service", "Service.onCreate called");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i("Service", "onBind called");
        try {
            Thread.sleep(900);
        } catch (InterruptedException e) {

        }
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i("Service", "onUnbind called");
        return false;
    }
}
